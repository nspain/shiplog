# Blog

This is the source of [my blog](https://shiplog.nickspain.dev). It's made with
[hugo](https://gohugo.io).

# Build

To run locally use:

``` bash
hugo serve -D
```

From the root of this repository. This will show draft posts. The production
build that I've setup on [Vercel](https://vercel.com) will not show these.

# Update theme

The theme is added as a subtree because [Vercel](https://vercel.com) doesn't
handle submodules. It was created using:

``` bash
git remote add -f tale-hugo https://github.com/EmielH/tale-hugo
git subtree add --prefix themes/tale-hugo tale-hugo master --squash
```

To update it do:

``` bash
git fetch tale-hugo master
git subtree pull --prefix themes/tale-hugo tale-hugo master --squahs
```
