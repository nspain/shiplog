# About Me

Hi! I'm Nick and I'm a Junior Software Engineer. This blog is a place for me to
express my thoughts and learnings. I'm still learning too so everything I say
should be taken with a grain of salt!

I've always go a few projects on the go. The ones I'm mainly that have me
particularly interested at the moment are:

- [unce](https://git.sr.ht/~nds/unce) - A DNS resolver and server.
- [sidre](https://git.sr.ht/~nds/sidre) - A SAML IdP intended for use in testing.

For other projects, depending on when I started the project you can find them
on:

- [sourcehut](https://git.sr.ht/~nds)
- [GitHub](https://github.com/nick96)
- [GitLab](https://gitlab.com/nspain)
