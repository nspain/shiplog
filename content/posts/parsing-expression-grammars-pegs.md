+++
date = 2020-06-01T02:50:03Z
draft = true
title = "Parsing Expression Grammars (PEGs)"

+++
* Another way of describing context free grammars (TODO: Check it's context free grammars) to BNF
* Mapping to code
  * Method per rule
  * Passing state between methods (use args and recursion rather than attributes)
* Testing
* Three stages
  * Tokenisation
  * Parsing (hardest)
  * Execution
  * Why separate them? Why join them?
    * Seems common to join tokenisation and parsing
    * 

# Resources

1. [https://craftinginterpreters.com/](https://craftinginterpreters.com/ "https://craftinginterpreters.com/")
2. [https://medium.com/@gvanrossum_83706/peg-parsing-series-de5d41b2ed60](https://medium.com/@gvanrossum_83706/peg-parsing-series-de5d41b2ed60 "https://medium.com/@gvanrossum_83706/peg-parsing-series-de5d41b2ed60")