---
title: Hugo on Vercel
date: 2020-05-28T14:00:00+00:00
tags:
- JAMStack
- blog
- hugo

---
What better first post on my blog then how I got it up and running!

I've been hearing a lot of good things about [Vercel](https://vercel.com)
recently. It's a platform to easily deploy the frontend part of your app,
basically the J and M in [JAMStack](https://jamstack.org). So I decided to build
my blog on that. [Hugo](https://gohugo.io) is a really nice and _super fast_ static site
generator that I've played around with before.

It took about an hour, maybe two, of playing around to get things up and
running. The main issue that I had was with Hugo themes (I'm using
[Tale](https://github.com/EmielH/tale-hugo) by Emiel Hollander). The recommended
way for using themes in Hugo seems to be adding the theme as a git submodule.
Submodules are always a bit of a pain, least of all because when you clone the
repo you have to do some extra steps to ensure the submodules are included. For
this reason the build on Vercel failed because Hugo couldn't find the specified
theme.

```
18:54:42.641 Error: module "tale-hugo" not found; either add it as a Hugo Module or store it in "/vercel/64840f48/themes".: module does not exist
18:54:42.644 Error: Command "hugo -D --gc" exited with 255
```

So instead I used
[subtrees](https://www.atlassian.com/git/tutorials/git-subtree).

# Steps

You'll need:

- [Hugo](https://gohugo.io), follow the relevant instructions on their [Install
  Hugo](https://gohugo.io/getting-started/installing/) page. 
  
  **Note:** I ended
  up just doing `go get github.com/gohugoio/hugo` because it's the easiest way
  to get the latest and I already had Go installed.
- [git](https://git-scm.com/)
- git-subtree, this may come installed when you install git. I'm on Fedora so
  had to install it via `sudo dnf install git-subtree`
  

## Setup your Vercel account

This ones pretty easy, just go to [Vercel](https://vercel.com) and setup an
account. Once your account is setup, create a project called "blog". Connect
Vercel to the git forge you'll be hosting your blog source on. I'm using GitLab
but you can also use GitHub or BitBucket.

## Initialise your Hugo blog

With Hugo installed you can create the directory your blog will be in using:

``` bash
hugo new blog
```

This will create a `blog` directory in your working directory with a skeleton
Hugo project.

You'll also need to initialise a git repository in the root directory. Do this using:

``` bash
git init .
```

## Add your theme

Have a look at the themes over at https://themes.gohugo.io/. There are some
pretty nice once. I ended up going with
[tale-hugo](https://github.com/EmielH/tale-hugo), it's nice and minimalist. Once
you've settle on a theme, we're going to add it to the repository. For this
you'll need the URL of where the theme's source is hosted. For me that's
`https://github.com/EmielH/tale-hugo`.

As I said before, we're going to add the theme as a subtree to avoid the pain of
submodules and allow Vercel to build our blog. To do this we first add the theme
as a remote, this isn't 100% necessary but it makes it easier to refer to the
theme when we want to update it. To add the remote we do:

``` bash
git remote add -f tale-hugo https://github.com/EmielH/tale-hugo
```

The `-f` flag `fetch`es the repository for us.

We then add the theme as a subtree:

``` bash
git subtree add --prefix themes/tale-hugo tale-hugo master --squash
```

**Note:** To update the theme, do:

``` bash
git subtree pull --prefix themes/tale-hugo tale-hugo master --squash
```

You'll need to add `theme = "tale-hugo"` to the `config.toml` file in the root
directory. Also, have a look at the theme's install instructions as some of them
require some configuration.

With all the snippets in this section, if you're not using `tale-hugo`, replace
`tale-hugo` with your theme's name and `https://github.com/EmielH/tale-hugo`
with the URL to where your theme is hosted.

## Deploy your site

As we connected Vercel to your git forge in [step 1](#setup-your-vercel-account)
to deploy your blog, all you have to do is push it to your git forge and point
your Vercel project (we called it "blog") to that particular repository, your
site will then be live! Vercel will deploy it to `https://blog-<random
string>.now.sh` but you can add custom domains (which is what I've done).

Hugo is so fast that the site is normally up within 7 seconds of Vercel picking
up the build!