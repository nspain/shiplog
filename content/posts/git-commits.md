---
title: Git Commits
date: 2020-05-28T14:00:00.000+00:00
draft: true

---
These are my thoughts on git commits and git etiquette in general. I always try to maintain good git etiquette because it's central to me being able manage the code base I'm working on well.

# Messages

Commit messages are the history of what has happened on your project. They're you're window into what past developers where doing and will the the window for future developers to see what you were doing. Good commit messages make this history clear, bad ones obscure it and make the story difficult to unwind.

The first part of the commit messages is its subject line. The key thing I've noticed about good commit messages, and something that I endeavour to do in mine, is that they're higher level than just _what_ was done. You can go into much more detail in the body but the subject line is what people see when perusing commit messages. When I'm bisecting looking for a bug, high level commit messages make things much simpler. They do need to be more descriptive than "Fixed bug in x" of "Updated x" though, I see these all the time and it drives me nuts! Once you've got some idea of what the bug is, you often want to find the commit that cause the bug. This is much easier if you can look back through the commit history and pick out relevant commits by their subject line, like when you're scanning your emails.

Another point that makes it easier to search the commit log is prefixing commit messages with the sub component (this is done, for example, in the Linux kernel). Prefixing isn't always possible if your change touches multiple sub-components but you definitely still want a descriptive commit message because that's a spot where it's much easier to introduce subtle bugs. Prefixing commit messages means you can easily grep for commits to that sub-component; `git log --grep '<prefix>'`. In simple cases you could achieve this using `git log --follow='<component-dir>'` but having it in the commit subjects makes you instantly aware that it refers to that sub-component. 

# Notes

[https://www.kernel.org/doc/html/v4.10/process/submitting-patches.html](https://www.kernel.org/doc/html/v4.10/process/submitting-patches.html "https://www.kernel.org/doc/html/v4.10/process/submitting-patches.html")

[http://who-t.blogspot.com/2009/12/on-commit-messages.html](https://www.kernel.org/doc/html/v4.10/process/submitting-patches.html "https://www.kernel.org/doc/html/v4.10/process/submitting-patches.html")

# Messages

* Project history
* Make it easier to find where an issue was introduced
* Use the imperative at least in the subject line
* Consistent with generated commit messages
* Prefix with such component
* Makes it easier to search for commits
* Writing subject line like SEO

# Atomic commits

* Can be rolled back and the build still works
* Can still see reasoning why a choice was made
* In depth commit messages
* Squash commits that don't add anything to the history
* E.g. tweaking of a build file
* Can be multiple commits for a large feature
* Just like multiple functions make up a feature
* Potentially squash commits before putting into master
* Keep each commit in the message and have an over arching subject line